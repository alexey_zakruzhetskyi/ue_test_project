// Copyright Epic Games, Inc. All Rights Reserved.


#include "GeometrySandbox2GameModeBase.h"
#include "SandboxPawn.h"

AGeometrySandbox2GameModeBase::AGeometrySandbox2GameModeBase()
{
	DefaultPawnClass = ASandboxPawn::StaticClass();
}
